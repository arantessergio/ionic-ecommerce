import { Component } from '@angular/core';
import { IonicPage, Nav, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { HomePage } from '../home/home';



@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  loading: boolean = false;

  email: string;
  password: string;
  retypePassword: string;

  constructor(public nav: Nav, public navParams: NavParams, private global: GlobalProvider) {}

  async save() {

    try {

      const result: any = await this.global.post(`users`, {
        email: this.email,
        password: this.password
      });
  
      if (result) {

        if (result.success === false) {

          this.global.toastPresenter('Já existe um usuário cadastrado com este email!');

        } else {

          this.nav.setRoot(HomePage);
    
          this.loading = false;
    
          this.global.toastPresenter('Usuário cadastrado com sucesso, bem vindo!');
  
          window.localStorage.setItem('lojinha-user', JSON.stringify(result));

        }
  
        
      }
      
    } catch (err) {
      
      this.global.toastPresenter('Ocorreu um erro ao processar sua solicitação!');

    }

    
  }

  canSignup() {
    return this.email && this.email !== '' &&
      this.password && this.password !== '' &&
      this.retypePassword && this.retypePassword !== '' &&
      this.password === this.retypePassword;
  }

}
