import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';

@Injectable()
export class GlobalProvider {

  public BASE_URL: string = 'http://localhost:9000';
  headers: HttpHeaders;

  constructor(public http: HttpClient, private toastCtrl: ToastController) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-type', 'application/json');
  }

  public get(sufix) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.BASE_URL}/${sufix}`)
        .subscribe(data => {
          resolve(data);
        }, err => {
          reject(err);
        });
    });
  }

  public post(sufix, body) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.BASE_URL}/${sufix}`, body, {headers: this.headers})
        .subscribe(data => {
          resolve(data);
        }, err => {
          reject(err);
        });
    });
  }

  public put(sufix, body) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.BASE_URL}/${sufix}`, body)
        .subscribe(data => {
          resolve(data);
        }, err => {
          reject(err);
        });
    });
  }

  public delete(sufix) {
    return new Promise((resolve, reject) => {
      this.http.delete(`${this.BASE_URL}/${sufix}`)
        .subscribe(data => {
          resolve(data);
        }, err => {
          reject(err);
        });
    });
  }

  public getUserId() {
    const item = window.localStorage.getItem('lojinha-user');

    return item ? JSON.parse(item).id : null;
  }

  public toastPresenter(message) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });

    toast.present();
  }

}
