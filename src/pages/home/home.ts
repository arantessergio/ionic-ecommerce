import { Component } from '@angular/core';
import { Nav, Events, ActionSheetController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { CartPage } from '../cart/cart';
import { ProductDetailsPage } from '../product-details/product-details';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  loading: boolean = false;
  products: any = [];
  cartItems: Array<any> = [];

  constructor(public nav: Nav, private global: GlobalProvider,
    private events: Events, private actionSheetCtrl: ActionSheetController) {
      this.events.subscribe('fetch-cart', () => {
        this.loadCart();
      });
    }

  ionViewDidLoad() {
    this.fetchProducts();
    this.loadCart();
  }

  async fetchProducts() {

    try {
      this.loading = true;

      const result = await this.global.get('products');

      if (result) {

        this.products = result;

        this.loading = false;

      }
    } catch(err) {

      console.error(err);

      this.global.toastPresenter('Ocorreu um erro ao processar sua solicitação!');
      
    }
  }
  
  isCartAdded(id) {
    return this.cartItems.filter(item => item.product.id === id).length;
  }
  
  async loadCart() {
    
    try {
      const result: any = await this.global.get(`users/${this.global.getUserId()}/cart`);
      
      if (result && result.items) {
        
        this.cartItems = result.items;
        
      }
    } catch (err) {
      
      this.global.toastPresenter('Ocorreu um erro ao processar sua solicitação!');

    }
  }

  async viewProduct(id) {

    try {

      const product = await this.global.get(`products/${id}`);

      if (product) {

        this.nav.push(ProductDetailsPage, {product});

      }

    } catch(err) {

      console.error(err);
      this.global.toastPresenter('Ocorreu um erro ao processar sua solicitação, por favor, tente novamente!');

    }


  }

  openLogoutActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Deseja efetuar o log out?',
      buttons: [{
        text: 'Sim',
        role: 'destructive',
        handler: () => { this.logout() }
      }, {
        text: 'Não',
        role: 'cancel'
      }]
    });

    actionSheet.present();
  }

  logout() {
    
    window.localStorage.removeItem('lojinha-user');

    this.nav.setRoot(LoginPage);

  }

  goToCart() {

    this.nav.push(CartPage);

  }

}
