import { Component } from '@angular/core';
import { IonicPage, Nav, NavParams, Events } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';


@IonicPage()
@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
})
export class ProductDetailsPage {

  product: any;
  loading: boolean = false;
  amount: any = 0;
  total: any = 0;

  constructor(public nav: Nav, public navParams: NavParams,
    private global: GlobalProvider, private events: Events) {
    this.product = navParams.get('product');
  }

  ionViewDidLoad() {

  }

  calculateTotal($event) {
    this.total = this.product.price * this.amount;
  }

  async addToCart() {
    
    this.loading = true;

    try {
      const result = await this.global.post(`users/${this.global.getUserId()}/cart`, {
        product: this.product,
        amount: this.amount,
        total: this.total
      });
    
      this.global.toastPresenter('Item inserido com sucesso!');

      this.events.publish('fetch-cart');
      
      this.loading = false;
      
      this.nav.pop();

    } catch (err) {

      console.error(err);
      this.loading = true;
      this.global.toastPresenter('Ocorreu um erro ao tentar inserir este item no carrinho!');

    }

  }

}
