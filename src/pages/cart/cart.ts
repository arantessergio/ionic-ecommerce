import { Component } from '@angular/core';
import { IonicPage, Nav, NavParams, Events } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';


@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  items: Array<any> = [];
  loading: boolean = false;

  constructor(public nav: Nav, public navParams: NavParams,
    private global: GlobalProvider, private events: Events) {
  }

  ionViewDidLoad() {
    this.fetchItems();
  }

  async fetchItems() {
    try {
      const result: any = await this.global.get(`users/${this.global.getUserId()}/cart`);
  
      if (result) {
        this.items = result.items;
      }

    } catch (err) {

      console.error(err);
      this.global.toastPresenter('Ocorreu um erro ao processar sua solicitação!')

    }
    
  }

  async removeItem(id) {

    try {
      
      const result = await this.global.delete(`items/${id}`);

      this.fetchItems();
      this.global.toastPresenter('Item removido com sucesso!');
      this.events.publish('fetch-cart');
  
    } catch (err) {

      console.error(err);
      this.global.toastPresenter('Ocorreu um erro ao tentar remover este item.');

    }


  }

  async finishOrder() {

    try {

      this.loading = true;

      const result = await this.global.post(`users/${this.global.getUserId()}/cart/finish`, {});

      if (result) {
        
        this.global.toastPresenter('Operação realizada com sucesso, verifique a sua caixa de entrada!');
  
        this.events.publish('fetch-cart');
  
        this.fetchItems();

        this.loading = false;

      }


    } catch (err) {

      this.loading = false;

      this.global.toastPresenter('Ocorreu um erro ao processar a sua solicitação.');

    }
    
    

  }

}
