import { Component } from '@angular/core';
import { IonicPage, Nav, NavParams, ToastController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string;
  password: string;

  constructor(public nav: Nav, public navParams: NavParams,
    private global: GlobalProvider, private toastCtrl: ToastController) {}

  ionViewDidLoad() {}

  async login() {
    
    try {

      const result: any = await this.global.post('users/auth', {
        email: this.email,
        password: this.password
      });

      if (result) {

        window.localStorage.setItem('lojinha-user', JSON.stringify(result));
        
        this.nav.setRoot(HomePage);
        
        this.toastPresenter(`Bem vindo ${result.email}!`);
        
      }
      
    } catch (err) {
      
      this.toastPresenter('Ocorreu um erro ao processar sua solicitação!');

    } 
    
  }

  canLogin() {
    return this.email && this.email !== '' && this.password && this.password !== '';
  }

  signup() {
    this.nav.push(SignupPage);
  }

  toastPresenter(message) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });

    toast.present();
  }



}
